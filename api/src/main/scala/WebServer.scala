import akka.actor.{Props, ActorSystem}
import akka.http.scaladsl.Http
import akka.http.scaladsl.model.StatusCodes
import akka.stream.ActorMaterializer
import akka.http.scaladsl.server.Route
import akka.http.scaladsl.server.Directives._

import scala.io.StdIn

object WebServer {
  def main(args: Array[String]) {

    implicit val system = ActorSystem("MBot")
    implicit val materializer = ActorMaterializer()
    val communication = system.actorOf(Props[Communication])
    communication ! Move(Protocol.Direction.Stop)

    val route: Route =
        post {
          path("action") {
            entity(as[String]) { action =>
              action match  {
                case "Up"=>
                  communication ! Move(Protocol.Direction.Forward)
                case "Down" =>
                  communication ! Move(Protocol.Direction.Backward)
                case "Right" =>
                  communication ! Move(Protocol.Direction.Right)
                case "Left" =>
                  communication ! Move(Protocol.Direction.Left)
                case "Stop" =>
                  communication ! Move(Protocol.Direction.Stop)
              }
              complete(StatusCodes.OK)
            }
          }
        }
    implicit val executionContext = system.dispatcher
    val bindingFuture = Http().bindAndHandle(route, "0.0.0.0", 8080)
    bindingFuture
      .flatMap(_.unbind()) // trigger unbinding from the port
      .onComplete(_ ⇒ system.terminate()) // and shutdown when done
  }
}