import org.scalajs.dom.ext.Ajax
import org.scalajs.dom.raw.KeyboardEvent

import scala.scalajs.js.JSApp
import scala.scalajs.js.annotation.JSExport


object UiApp extends JSApp {

  private val baseUrl = "http://192.168.1.235:8080"
  private val actionUrl = s"$baseUrl/action"

  def main(): Unit = {
    import org.scalajs.dom
    import scala.concurrent .ExecutionContext.Implicits.global
    dom.document.onkeydown =  { (event: KeyboardEvent) =>
     convertMap.get(event.keyCode).foreach{(direction: String) => Ajax.post(actionUrl, direction)}
    }
    dom.document.onkeyup =  { (event: KeyboardEvent) =>
      convertMap.get(event.keyCode).foreach{_ => Ajax.post(actionUrl, "Stop")}
    }
  }

  @JSExport
  def clicked(button: String): Unit = {
    println(button)
  }


  private val convertMap = Map (
    38 -> "Up",
    40 -> "Down",
    37 -> "Left",
    39 -> "Right"
  )
}