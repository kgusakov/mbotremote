import sbt.Keys._

scalaVersion := "2.11.8"

name := "mBotRemote"

lazy val communication = (project in file("communication")).
  settings(Commons.settings: _*).
  settings(
    libraryDependencies ++= Seq("com.github.jodersky" % "flow-core_2.11" % "2.6.0",
                                "com.github.jodersky" % "flow-native" % "2.6.0" % "runtime")
  )

lazy val api = (project in file("api")).
  settings(Commons.settings: _*).
  settings(
    libraryDependencies ++= Seq("com.typesafe.akka" %% "akka-http-core" % "2.4.4",
                                "com.typesafe.akka" %% "akka-http-experimental" % "2.4.4")
  ).
  dependsOn(communication)

lazy val gui = (project in file("gui")).
  dependsOn(communication).
  settings(Commons.settings: _*)

lazy val web_ui = (project in file("web-ui")).
  settings(Commons.settings: _*).
  settings(
    libraryDependencies += "org.scala-js" %%% "scalajs-dom" % "0.9.0"
  ).
  enablePlugins(ScalaJSPlugin)

