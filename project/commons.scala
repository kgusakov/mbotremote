import sbt._
import sbt.Keys._

object Commons {
  val appVersion = "1.0"

  val settings: Seq[Def.Setting[_]] = Seq(
    version := appVersion,
    resolvers += Resolver.jcenterRepo,
    scalaVersion := "2.11.8"
  )
}