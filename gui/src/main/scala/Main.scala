import javafx.application.Application
import javafx.event.EventHandler
import javafx.scene.Scene
import javafx.scene.input.KeyEvent
import javafx.scene.layout.StackPane
import javafx.stage.Stage

import akka.actor.{Props, ActorSystem}


class Main extends Application {

  val system = ActorSystem("MBot")
  val communication = system.actorOf(Props[Communication])

  override def start(primaryStage: Stage): Unit = {
    val root = new StackPane()
    val scene = new Scene(root, 300, 250)
    scene.addEventHandler(KeyEvent.KEY_PRESSED, new EventHandler[KeyEvent] {
      override def handle(event: KeyEvent): Unit = {
        if (event.getCode.isArrowKey) {
          event.getCode.getName match  {
            case "Up"=>
              communication ! Move(Protocol.Direction.Forward)
            case "Down" =>
              communication ! Move(Protocol.Direction.Backward)
            case "Right" =>
              communication ! Move(Protocol.Direction.Right)
            case "Left" =>
              communication ! Move(Protocol.Direction.Left)
          }
        }

      }
    })
    scene.addEventHandler(KeyEvent.KEY_RELEASED, new EventHandler[KeyEvent] {
      override def handle(event: KeyEvent): Unit = {
        if (event.getCode.isArrowKey) {
          communication ! Move(Protocol.Direction.Stop)
        }

      }
    })

    primaryStage.setScene(scene)
    primaryStage.show()
  }
}

object Main {
  def main(args: Array[String]): Unit = {
    Application.launch(classOf[Main], args:_*)
  }
}
