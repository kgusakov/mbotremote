import akka.actor.{ActorRef, Stash, ActorLogging, Actor}
import akka.io.IO
import akka.util.ByteStringBuilder
import com.github.jodersky.flow.{AccessDeniedException, Serial, Parity, SerialSettings}


case class Move(direction: Char)

class Communication extends Actor with Stash with ActorLogging {

  var operator: Option[ActorRef] = None

  val port = "/dev/rfcomm0"
  val settings = SerialSettings(
    baud = 115200,
    characterSize = 8,
    twoStopBits = false,
    parity = Parity.None
  )

  @throws[Exception](classOf[Exception])
  override def preStart(): Unit = {
    super.preStart()
    implicit val system = context.system
    IO(Serial) ! Serial.Open(port, settings)
  }

  override def receive: Receive = {
    case Serial.CommandFailed(cmd: Serial.Open, reason: AccessDeniedException) =>
      println("You're not allowed to open that port!")
    case Serial.CommandFailed(cmd: Serial.Open, reason) =>
      println("Could not open port for some other reason: " + reason.getMessage)
    case Serial.Opened(settings) => {
      println("Success!")
      operator = Option(sender)
      println("It's time to move!")
      unstashAll()
    }
    case Move(direction) =>
      operator.fold(stash())(_ ! Serial.Write(new ByteStringBuilder().putByte(direction.toByte).result))
    case Serial.Received(data) => {
      println("Received: " + data)
    }
  }
}

object Protocol {
  object Direction {
    val Forward: Char = 'F'
    val Right: Char = 'R'
    val Backward: Char = 'B'
    val Left: Char = 'L'
    val Stop: Char = 'S'
  }
}